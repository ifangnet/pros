<?php

namespace We7\V217;

defined('IN_IA') or exit('Access Denied');
/**
* [WeEngine System] Copyright (c) 2014 W7.CC
* Time: 1580799307
* @version 2.1.7
*/

class UpdateUniAccount {

	/**
	 *  执行更新
	 */
	public function up() {
		if(!pdo_fieldexists('uni_account', 'logo')) {
			pdo_query("ALTER TABLE " . tablename('uni_account') . " ADD `logo` varchar(250) NOT NULL DEFAULT '' ;");
			pdo_query("ALTER TABLE " . tablename('uni_account') . " ADD `qrcode` varchar(250) NOT NULL DEFAULT '' ;");
			$uni_accounts = table('uni_account')->select(array('uniacid', 'default_acid'))->getall();
			foreach ($uni_accounts as $uni_account) {
				table('uni_account')
					->fill(array(
						'logo' => to_global_media('headimg_' . $uni_account['default_acid'] . '.jpg'),
						'qrcode' => to_global_media('qrcode_' . $uni_account['default_acid'] . '.jpg')
					))
					->where('uniacid', $uni_account['uniacid'])
					->save();
			}
		}
	}

	/**
	 *  回滚更新
	 */
	public function down() {


	}
}
