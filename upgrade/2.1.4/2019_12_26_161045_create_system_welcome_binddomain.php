<?php

namespace We7\V214;

defined('IN_IA') or exit('Access Denied');
/**
* [WeEngine System] Copyright (c) 2014 W7.CC
* Time: 1577347845
* @version 2.1.4
*/

class CreateSystemWelcomeBinddomain {

/**
 *  执行更新
 */
public function up() {
	if (!pdo_tableexists('system_welcome_binddomain')) {
		pdo_run("CREATE TABLE `ims_system_welcome_binddomain` ( `id` INT(10) NOT NULL AUTO_INCREMENT , `uid` INT(10) NOT NULL COMMENT '用户UID' , `module_name` VARCHAR(50) NOT NULL COMMENT '模块标识' , `domain` VARCHAR(50) NOT NULL COMMENT '绑定的域名' , `createtime` INT(10) NOT NULL , `updatetime` INT(10) NOT NULL , PRIMARY KEY (`id`), INDEX (`module_name`), INDEX (`uid`), INDEX (`domain`)) ENGINE = InnoDB CHARSET=utf8 COLLATE utf8_general_ci COMMENT = '首页应用绑定域名表';");
	}
}

/**
 *  回滚更新
 */
public function down() {


}
}
