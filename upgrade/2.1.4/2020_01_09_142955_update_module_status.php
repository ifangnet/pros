<?php

namespace We7\V214;

defined('IN_IA') or exit('Access Denied');
/**
* [WeEngine System] Copyright (c) 2014 W7.CC
* Time: 1578551395
* @version 2.1.4
*/

class UpdateModuleStatus {

	/**
	 *  执行更新
	 */
	public function up() {
		if (pdo_fieldexists('modules_cloud', 'module_status')) {
			table('modules_cloud')->searchWithUninstall(MODULE_CLOUD_UNINSTALL)->where('module_status', 0)->fill('module_status', 1)->save();
		}
	}

	/**
	 *  回滚更新
	 */
	public function down() {


	}
}
