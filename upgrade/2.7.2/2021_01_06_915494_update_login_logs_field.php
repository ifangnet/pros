<?php

namespace We7\V272;

defined('IN_IA') or exit('Access Denied');
/**
 * [WeEngine System] Copyright (c) 2014 W7.CC
 * Time: 1609915494
 * @version 2.7.2
 */

class UpdateLoginLogsField {

	/**
	 *  执行更新
	 */
	public function up() {
		if (!pdo_fieldexists('users_login_logs', 'login_at')) {
			pdo_query("ALTER TABLE " . tablename('users_login_logs') . " CHANGE `login_at` `createtime` INT(10) UNSIGNED NOT NULL");
		}
	}

	/**
	 *  回滚更新
	 */
	public function down() {


	}
}
