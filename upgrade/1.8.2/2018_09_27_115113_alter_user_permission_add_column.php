<?php

namespace We7\V182;

defined('IN_IA') or exit('Access Denied');
/**
 * [WeEngine System] Copyright (c) 2014 W7.CC
 * Time: 1538020273
 * @version 1.8.2
 */

class AlterUserPermissionAddColumn {

	/**
	 *  执行更新
	 */
	public function up() {
		if (!pdo_fieldexists('users_permission', array('modules', 'templates'))) {
			$table_name = tablename('users_permission');
			$sql = <<<EOT
				ALTER TABLE {$table_name} ADD `modules` text NOT NULL DEFAULT '' ;
				ALTER TABLE {$table_name} ADD `templates` text NOT NULL DEFAULT '' ;
EOT;
			pdo_run($sql);
		}
	}
	
	/**
	 *  回滚更新
	 */
	public function down() {
		

	}
}
		