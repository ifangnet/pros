<?php

namespace We7\V181;

defined('IN_IA') or exit('Access Denied');
/**
 * [WeEngine System] Copyright (c) 2014 W7.CC
 * Time: 1536053645
 * @version 1.8.1
 */

class AlterSiteStoreGoods {

	/**
	 *  执行更新
	 */
	public function up() {
		if(!pdo_fieldexists('site_store_goods', 'user_group')) {
			pdo_query("ALTER TABLE " . tablename('site_store_goods') . " ADD `user_group` INT(10) NOT NULL DEFAULT 0;");
		}
	}
	
	/**
	 *  回滚更新
	 */
	public function down() {
		

	}
}
		