<?php

	namespace We7\V218;

	defined('IN_IA') or exit('Access Denied');
	/**
	 * [WeEngine System] Copyright (c) 2014 W7.CC
	 * Time: 1588062888
	 * @version 2.1.8
	 */

	class UpdateSiteTemplate {

		/**
		 *  执行更新
		 */
		public function up() {
			if (pdo_fieldexists('mc_oauth_fans', 'acid')) {
				$sql = 'ALTER TABLE ' . tablename('mc_oauth_fans') . " CHANGE `acid` `uniacid` INT(10) UNSIGNED NOT NULL";
				pdo_query($sql);
			}
		}

		/**
		 *  回滚更新
		 */
		public function down() {


		}
	}
