<?php

namespace We7\V2710;

defined('IN_IA') or exit('Access Denied');
/**
* [WeEngine System] Copyright (c) 2014 W7.CC
* Time: 1634542200
* @version 2.7.10
*/

class UpdateModulesCloud {

	/**
	 *  执行更新
	 */
	public function up() {
		if (!pdo_fieldexists('modules_cloud', 'service_expire_time')) {
			pdo_query("ALTER TABLE " . tablename('modules_cloud') . " ADD `service_expire_time` INT( 11 ) NOT NULL DEFAULT  0;");
		}
		if (!pdo_fieldexists('modules_cloud', 'system_shutdown_time')) {
			pdo_query("ALTER TABLE " . tablename('modules_cloud') . " ADD `system_shutdown_time` INT( 11 ) NOT NULL DEFAULT  0;");
		}
		if (!pdo_fieldexists('modules_cloud', 'status')) {
			pdo_query("ALTER TABLE " . tablename('modules_cloud') . " ADD `status` TINYINT( 1 ) NOT NULL DEFAULT  1;");
		}
	}

	/**
	 *  回滚更新
	 */
	public function down() {
	}
}
