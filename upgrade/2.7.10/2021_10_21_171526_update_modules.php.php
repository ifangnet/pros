<?php

namespace We7\V2710;

defined('IN_IA') or exit('Access Denied');
/**
* [WeEngine System] Copyright (c) 2014 W7.CC
* Time: 1634807726
* @version 2.7.10
*/

class UpdateModules {

	/**
	 *  执行更新
	 */
	public function up() {
		if (!pdo_fieldexists('modules', 'code')) {
			pdo_query("ALTER TABLE " . tablename('modules') . " ADD `code` VARCHAR( 10 ) NOT NULL AFTER `from`;");
		}
	}

	/**
	 *  回滚更新
	 */
	public function down() {
	}
}
