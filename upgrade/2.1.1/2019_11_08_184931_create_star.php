<?php

namespace We7\V211;

defined('IN_IA') or exit('Access Denied');
/**
* [WeEngine System] Copyright (c) 2014 W7.CC
* Time: 1573210171
* @version 2.1.1
*/

class CreateStar {

/**
 *  执行更新
 */
public function up() {
	if (!pdo_tableexists('users_operate_star')) {
		$table_name = tablename('users_operate_star');
		$sql = <<<EOF
CREATE TABLE IF NOT EXISTS $table_name (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `type` tinyint(1) UNSIGNED NOT NULL COMMENT '标记星标的类型：1、平台账号；2、模块',
  `uid` int(10) UNSIGNED NOT NULL COMMENT '用户uid',
  `uniacid` int(10) UNSIGNED NOT NULL COMMENT '平台账号UNIACID',
  `module_name` varchar(100) NOT NULL COMMENT '模块名',
  `rank` int(10) NOT NULL COMMENT '显示顺序',
  `createtime` int(10) NOT NULL COMMENT '添加时间',
  PRIMARY KEY (`id`),
  KEY `type` (`type`),
  KEY `uid` (`uid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='用户操作星标表';
EOF;
		pdo_query($sql);
	}
}

/**
 *  回滚更新
 */
public function down() {


}
}
