<?php

namespace We7\V187;

defined('IN_IA') or exit('Access Denied');
/**
 * [WeEngine System] Copyright (c) 2014 W7.CC
 * Time: 1544600841
 * @version 1.8.7
 */

class CreateTableSiteStoreCashLog {

	/**
	 *  执行更新
	 */
	public function up() {
		if (!pdo_tableexists('site_store_cash_log')) {
			$table_name = tablename('site_store_cash_log');
			pdo_run(
				"CREATE TABLE {$table_name} (
					`id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
					`founder_uid` int(10) NOT NULL DEFAULT 0 COMMENT '副创始人uid',
					`number` char(30) NOT NULL DEFAULT '' COMMENT '流水号',
					`amount` decimal(10,2) NOT NULL DEFAULT 0 COMMENT '提现金额',
					`status` tinyint(1) NOT NULL DEFAULT 1 COMMENT '核销状态:1:待核销,2:已核销,3:已拒绝',
					`create_time` int(10) NOT NULL DEFAULT 0,
					PRIMARY KEY (`id`),
					KEY `founder_uid` (`founder_uid`)
				) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COMMENT='提现订单表';"
			);
		}
	}
	
	/**
	 *  回滚更新
	 */
	public function down() {
		

	}
}
		