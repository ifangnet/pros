<?php

namespace We7\V183;

defined('IN_IA') or exit('Access Denied');
/**
 * [WeEngine System] Copyright (c) 2014 W7.CC
 * Time: 1540179398
 * @version 1.8.3
 */

class CreateTableUsersExtraModules {

	/**
	 *  执行更新
	 */
	public function up() {
		if (!pdo_tableexists('users_extra_modules')) {
			$table_name = tablename('users_extra_modules');
			$sql = <<<EOF
CREATE TABLE $table_name (
	`id` int(10) unsigned not null AUTO_INCREMENT,
	`uid` int(10) unsigned not null comment '用户id',
	`module_name` varchar(100) not null comment '模块标识',
	PRIMARY KEY(`id`)
) DEFAULT CHARSET=utf8;
EOF;
			pdo_query($sql);
		}
	}
	
	/**
	 *  回滚更新
	 */
	public function down() {
		

	}
}
		