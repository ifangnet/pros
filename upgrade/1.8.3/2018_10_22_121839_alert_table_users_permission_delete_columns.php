<?php

namespace We7\V183;

defined('IN_IA') or exit('Access Denied');
/**
 * [WeEngine System] Copyright (c) 2014 W7.CC
 * Time: 1540181919
 * @version 1.8.3
 */

class AlertTableUsersPermissionDeleteColumns {

	/**
	 *  执行更新
	 */
	public function up() {
		if (pdo_fieldexists('users_permission', 'modules')) {
			pdo_query("ALTER TABLE " . tablename('users_permission') . " DROP modules");
		}

		if (pdo_fieldexists('users_permission', 'templates')) {
			pdo_query("ALTER TABLE " . tablename('users_permission') . " DROP templates");
		}
	}
	
	/**
	 *  回滚更新
	 */
	public function down() {
		

	}
}
		