<?php

namespace We7\V188;

defined('IN_IA') or exit('Access Denied');
/**
 * [WeEngine System] Copyright (c) 2014 W7.CC
 * Time: 1547276096
 * @version 1.8.8
 */

class AlterTableModulesCloudAddBuytime {

	/**
	 *  执行更新
	 */
	public function up() {
		if(!pdo_fieldexists('modules_cloud', 'buytime')) {
			pdo_query("ALTER TABLE " . tablename('modules_cloud') . " ADD `buytime` int(10) NOT NULL DEFAULT 0 COMMENT '购买时间';");
		}
	}
	
	/**
	 *  回滚更新
	 */
	public function down() {
		

	}
}
		