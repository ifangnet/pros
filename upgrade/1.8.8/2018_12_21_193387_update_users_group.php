<?php

namespace We7\V188;

defined('IN_IA') or exit('Access Denied');
/**
 * [WeEngine System] Copyright (c) 2014 W7.CC
 * Time: 1545305615
 * @version 1.8.8
 */

class UpdateUsersGroup {

	/**
	 *  执行更新
	 */
	public function up() {
		if(!pdo_fieldexists('users_group', 'maxbaiduapp')) {
			pdo_query("ALTER TABLE " . tablename('users_group') . " ADD `maxbaiduapp` int(10) NOT NULL DEFAULT 0 COMMENT '百度小程序最大创建数量';");
		}
		if(!pdo_fieldexists('users_group', 'maxtoutiaoapp')) {
			pdo_query("ALTER TABLE " . tablename('users_group') . " ADD `maxtoutiaoapp` int(10) NOT NULL DEFAULT 0 COMMENT '头条最大创建数量';");
		}
	}
	
	/**
	 *  回滚更新
	 */
	public function down() {
		

	}
}
		