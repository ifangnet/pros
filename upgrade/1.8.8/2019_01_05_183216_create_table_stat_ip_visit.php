<?php

namespace We7\V188;

defined('IN_IA') or exit('Access Denied');
/**
 * [WeEngine System] Copyright (c) 2014 W7.CC
 * Time: 1546684336
 * @version 1.8.8
 */

class CreateTableStatIpVisit {

	/**
	 *  执行更新
	 */
	public function up() {
		if(!pdo_tableexists('stat_ip_visit')) {
			$tablename = tablename('stat_ip_visit');
			$sql = "CREATE TABLE $tablename (
					`id` int(10) unsigned NOT NULL AUTO_INCREMENT,
					`ip` int(10) unsigned NOT NULL COMMENT 'ip',
					`uniacid` int(10) NOT NULL,
					`type` varchar(10) NOT NULL COMMENT 'web 后端 app 手机短 api 微信api',
					`module` varchar(100) NOT NULL,
					`date` int(10) unsigned NOT NULL,
					PRIMARY KEY (`id`),
					KEY `ip` (`ip`) USING BTREE,
					KEY `date` (`date`) USING BTREE,
					KEY `module` (`module`) USING BTREE,
					KEY `uniacid` (`uniacid`) USING BTREE
				) ENGINE=InnoDB DEFAULT CHARSET=utf8";
			pdo_run($sql);
		}
	}
	
	/**
	 *  回滚更新
	 */
	public function down() {
		

	}
}
		