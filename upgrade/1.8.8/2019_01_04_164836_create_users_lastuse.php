<?php

namespace We7\V188;

defined('IN_IA') or exit('Access Denied');
/**
 * [WeEngine System] Copyright (c) 2014 W7.CC
 * Time: 1546505315
 * @version 1.8.8
 */

class CreateUsersLastuse {

	/**
	 *  执行更新
	 */
	public function up() {
		if(!pdo_tableexists('users_lastuse')) {
			$sql = "CREATE TABLE `ims_users_lastuse` (
				`id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
				`uid` int(10) NOT NULL,
				`uniacid` int(10) DEFAULT '0',
				`modulename` varchar(100) DEFAULT '',
				`type` varchar(100) DEFAULT '' COMMENT '入口类型:应用入口module_display;平台账号入口:account_display;模块最后使用的平台账号:module_display_modulename',
				PRIMARY KEY (`id`)
				) ENGINE=InnoDB COMMENT='用户最后使用的账号/模块';";
			pdo_run($sql);
		}
	}
	
	/**
	 *  回滚更新
	 */
	public function down() {
		

	}
}
		