<?php

namespace We7\V188;

defined('IN_IA') or exit('Access Denied');
/**
 * [WeEngine System] Copyright (c) 2014 W7.CC
 * Time: 1550041800
 * @version 1.8.8
 */

class MigrateDataFromUniGroupFromUsersExtraModules {

	/**
	 *  执行更新
	 */
	public function up() {
		$user_uni_group_add = pdo_getall('uni_group', array('uid !=' => 0));
		if (!empty($user_uni_group_add)) {
			foreach($user_uni_group_add as $key => $val) {
				$modules = unserialize($val['modules']);

				if (empty($modules) || empty($val['uid'])) {
					continue;
				}

				foreach ($modules as $support_type => $module_names) {
					$support = $support_type == 'modules' ? 'account_support' : $support_type . '_support';

					if (empty($module_names)) {
						continue;
					}

					foreach ($module_names as $module_name) {
						$insert_data = array(
							'uid' => $val['uid'],
							'module_name' => $module_name,
							'support' => $support,
						);
						$module_exists = pdo_get('users_extra_modules', $insert_data);
						if (empty($module_exists)) {
							pdo_insert('users_extra_modules', $insert_data);
						}
					}
					pdo_delete('uni_group', array('uid' => $val['uid']));
				}
			}
		}
	}
	
	/**
	 *  回滚更新
	 */
	public function down() {
		

	}
}
		