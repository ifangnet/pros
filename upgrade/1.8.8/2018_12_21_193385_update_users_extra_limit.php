<?php

namespace We7\V188;

defined('IN_IA') or exit('Access Denied');
/**
 * [WeEngine System] Copyright (c) 2014 W7.CC
 * Time: 1545305615
 * @version 1.8.8
 */

class UpdateUsersExtraLimit {

	/**
	 *  执行更新
	 */
	public function up() {
		if(!pdo_fieldexists('users_extra_limit', 'maxbaiduapp')) {
			pdo_query("ALTER TABLE " . tablename('users_extra_limit') . " ADD `maxbaiduapp` int(10) NOT NULL DEFAULT 0 COMMENT '可创建的百度小程序数量';");
		}
		if(!pdo_fieldexists('users_extra_limit', 'maxtoutiaoapp')) {
			pdo_query("ALTER TABLE " . tablename('users_extra_limit') . " ADD `maxtoutiaoapp` int(10) NOT NULL DEFAULT 0 COMMENT '可创建的头条小程序数量';");
		}
	}
	
	/**
	 *  回滚更新
	 */
	public function down() {
		

	}
}
		